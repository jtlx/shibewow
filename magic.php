<?php
header('Content-type: image/jpeg');
session_start();

$filename = $_SESSION['filename'];
$filetype = $_SESSION['type'];

$watermark = $_SESSION['watermark'];

$wow=$_SESSION['wow'];
$wowa=explode(",",$wow);


$img_path= "upload/" . $filename;

$color_order = str_shuffle("12345678");

$coord_order = str_shuffle("01234567");

if($filetype == "image/jpeg" || $filetype == "image/jpg")
{
$im = imagecreatefromjpeg($img_path);
}

if($filetype == "image/png")
{
$im = imagecreatefrompng($img_path);
}



list($width, $height) = getimagesize($img_path);


$c1 = imagecolorallocate($im, 102, 255, 38); //green 
$c2 = imagecolorallocate($im, 100, 100, 100); //grey
$c3 = imagecolorallocate($im, 217, 113, 235); //pink
$c4 = imagecolorallocate($im, 235, 233, 113); //yellow
$c5 = imagecolorallocate($im, 65, 225, 250); //blue light
$c6 = imagecolorallocate($im, 227, 54, 54); //red
$c7 = imagecolorallocate($im, 72, 102, 233); //blue dark
$c8 = imagecolorallocate($im, 227, 169, 34); //orange

$watermark_color = imagecolorallocate($im, 255, 255, 255);

// +rand(-10,10);

$font = './cs.ttf';
$text = 'wow';

$x=array(5  ,5  ,50  ,5  ,50  ,50  ,10  ,50);

$y=array(5  ,55 ,5  ,30  ,30  ,55  ,80,  80);

$deviation=20;

$fontsize=round($width/30);


for($i=0;$i < count($wowa);$i++)
{
$coord_set = $coord_order[$i];
imagettftext($im, $fontsize, 0, ($width/100)*($x[$coord_set] + rand (0,$deviation)), $height/100*($y[$coord_set] + rand (0,$deviation)), ${'c'. $color_order[$i]}, $font, $wowa[$i]);
}

if($watermark == 'true')
{
imagettftext ( $im , 8 , 0 , $width-142 , $height-2 , $watermark_color  , "./tahoma.ttf" , "instashibe.cloudcontrolled.com" );
}

imagejpeg($im, "upload/a" . $filename);
imagejpeg($im);
?>

<!--

for($i=1; $i <= 5; $i++) {
    ${'file' . $i} = $i;
}

-->