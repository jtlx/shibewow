<!DOCTYPE html>
<meta name="viewport" content="width=300">
<html>
<head>
	<title>Insta-shibe</Title>
    <link rel="stylesheet" href="css/style.css" type="text/css" charset="utf-8">
	<meta name="description" content="The mobile-friendly shibe maeker. Create a shibe in seconds.">
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-45893739-1', 'instashibe.cloudcontrolled.com');
	ga('send', 'pageview');

	</script>
</head>


<body>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<div id="main-box">
	<div id="border">
		<div id="top-box">
			<div id="title-box">
				<div style="padding-top:8px;">so shibe</div>
			</div>
			<div id="image-box">
				<img src="shibe.png" height="80px;" width="80px;" alt="doge :o">
			</div>
		</div>
		<div id="form-box">
			<form action="upload_file.php" method="post" enctype="multipart/form-data">

			Image URL: <input type="text" name="url"> <br>
			OR <br>
			<input type="file" name="file" id="file"><br>
			Supported formats:jpg, jpeg, png<br>
			max. 5MB<br><br><br>

			Phrases: <input type="text" name="wow"> <br>(seperated by commas, max 8)<br><br>
			<label for="watermark"> include tiny watermark :)</label>
			<input type="checkbox" name="watermark" id="watermark" value="true" checked><br>
			<input id="wow-button" type="submit" name="submit" value=">> wow <<">
			</form>
		</div>
	</div>
	<div id="social-buttons">
		<a href="https://twitter.com/share" class="twitter-share-button like-button" data-count="none" data-dnt="true">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		<div class="fb-like like-button" data-href="http://instashibe.cloudcontrolled.com/" data-width="75" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
	</div>
</div>

<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">

<!-- End of StatCounter Code for Default Guide -->
</body>
</html>
