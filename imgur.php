<?php
session_start();
$client_id = "c99eac500e38a3d";
// imgur client id= c99eac500e38a3d

$image =file_get_contents("upload/a" . $_SESSION['filename']);


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Client-ID ' . $client_id ));
curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'image' => base64_encode($image) ));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$reply = curl_exec($ch);

curl_close($ch);

$reply = json_decode($reply);

// printf('<img height="180" src="%s" >', $reply->data->link);


if($reply->success == 1)
{
	echo "<h3>uploaded!</h3>";
}
echo "link:";
echo "<input type=\"text\" size=\"50\" value=\"" . $reply->data->link ."\">";
echo "<br>";
//printf('<img  src="%s" >', $reply->data->link);

// echo "<h3>API Debug</h3><pre>";
// var_dump($reply);
// echo $reply->success;

header("Location: ". $reply->data->link);

?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45893739-1', 'instashibe.cloudcontrolled.com');
  ga('send', 'pageview');

</script>